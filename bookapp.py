#!/usr/bin/env python3

import re

from bookdb import BookDB
import traceback

DB = BookDB()


def book(book_id):
    body = "<h1>a book with id {0}</h1>\r\n".format(book_id)
    d = DB.title_info(book_id)
    if d is None:
        raise NameError
    body += "<ul>\r\n"
    for k in d.keys():
        body += "<li>{0}: {1}</li>\r\n".format(k, d[k])
    body += "</ul>\r\n"
    return body


def books():
    body = "<h1>a list of books</h1>"
    body += "<ul>\r\n"
    for d in DB.titles():
        body += "<li><a href=""book/{0}"">{1}</a></li>\r\n".format(d['id'], d['title'])
    body += "</ul>\r\n"
    return body


def resolve_path(path):
    names = {
        '/' : 'books',
        '' : 'books',
        'book' : 'book',
        '/book' : 'book',
	}

    calls = {
        'books' : books,
        'book' : book,
    }

    path = path.split('/')
    # path = path.strip('/').split('/')
    if (len(path) == 2):
        fname = names[path[0]]
        fargs = None
    elif (len(path) == 3):
        fname = names[path[1]]
        fargs = path[2:]
    else:
        raise NameError

    try:
        func = calls[fname]
    except KeyError:
        raise NameError

    return func, fargs

def application(environ, start_response):
    headers = [('Content-type', 'text/html')]
    try:
        path = environ.get('PATH_INFO', None)
        if path is None:
            raise NameError
        func, args = resolve_path(path)
        if args is not None:
            body = func(*args)
        else:
            body = func()
        status = "200 OK"
    except NameError:
        status = "404 Not Found"
        body = "<h1>Not Found</h1>"
    except Exception:
        status = "500 Internal Server Error"
        body = "<h1>Internal Server Error</h1>"
        print(traceback.format_exc())
    finally:
        headers.append(('Content-length', str(len(body))))
        start_response(status, headers)

    # NOTE: if using python3, you _must_ return a list here
    return [body.encode('utf8')]
	
if __name__ == '__main__':
    from wsgiref.simple_server import make_server
    srv = make_server('localhost', 8080, application)
    srv.serve_forever()
